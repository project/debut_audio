<?php

function debut_audio_helpinjector_keys() {
  return array(
    'audio_node_form:field_audio_embed:0:embed' => array('module' => 'debut_audio', 'file' => 'Upload-an-audio-file-to-an-external-host'),
    'audio_node_form:title' => array('module' => 'debut_audio', 'file' => 'Posting-a-new-audio-file'),
    'features_admin_form:features:status:debut_audio' => array('module' => 'debut_audio', 'file' => 'Debut-Audio'),
  );

}

