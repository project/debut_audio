<?php

/**
 * Implementation of hook_context_default_contexts().
 */
function debut_audio_context_default_contexts() {
  $export = array();
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'audio-content-type';
  $context->description = 'An embedded audio content type and associated displays.';
  $context->tag = 'Multimedia';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'audio' => 'audio',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-audio-block_1' => array(
          'module' => 'views',
          'delta' => 'audio-block_1',
          'region' => 'right',
          'weight' => 0,
        ),
      ),
    ),
    'breadcrumb' => 'audio',
    'menu' => 'audio',
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('An embedded audio content type and associated displays.');
  t('Multimedia');

  $export['audio-content-type'] = $context;
  return $export;
}
