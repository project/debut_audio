<?php

/**
 * Implementation of hook_content_default_fields().
 */
function debut_audio_content_default_fields() {
  $fields = array();

  // Exported field: field_audio_embed
  $fields['audio-field_audio_embed'] = array(
    'field_name' => 'field_audio_embed',
    'type_name' => 'audio',
    'display_settings' => array(
      'weight' => '-4',
      'parent' => '',
      'label' => array(
        'format' => 'hidden',
      ),
      'teaser' => array(
        'format' => 'hidden',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'audio_audio',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'hidden',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'emaudio',
    'required' => '0',
    'multiple' => '0',
    'module' => 'emaudio',
    'active' => '1',
    'widget' => array(
      'audio_width' => '425',
      'audio_height' => '350',
      'audio_autoplay' => 0,
      'preview_width' => '425',
      'preview_height' => '350',
      'preview_autoplay' => 0,
      'thumbnail_width' => '120',
      'thumbnail_height' => '90',
      'thumbnail_default_path' => '',
      'providers' => array(
        'archive_audio' => 'archive_audio',
        'podcastalley' => 'podcastalley',
        'custom_url' => 0,
        'odeo' => 0,
        'podomatic' => 0,
      ),
      'default_value' => array(
        '0' => array(
          'embed' => '',
          'value' => '',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Embedded audio',
      'weight' => '-4',
      'description' => '',
      'type' => 'emaudio_textfields',
      'module' => 'emaudio',
    ),
  );

  // Exported field: field_image
  $fields['audio-field_image'] = array(
    'field_name' => 'field_image',
    'type_name' => 'audio',
    'display_settings' => array(
      'weight' => '-3',
      'parent' => '',
      'label' => array(
        'format' => 'hidden',
      ),
      'teaser' => array(
        'format' => 'thumbnail_default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'hidden',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'thumbnail_default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'image_plain',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'filefield',
    'required' => '0',
    'multiple' => '0',
    'module' => 'filefield',
    'active' => '1',
    'list_field' => '0',
    'list_default' => 1,
    'description_field' => '0',
    'widget' => array(
      'file_extensions' => 'png gif jpg jpeg',
      'file_path' => 'images',
      'progress_indicator' => 'bar',
      'max_filesize_per_file' => '',
      'max_filesize_per_node' => '',
      'max_resolution' => '3000x3000',
      'min_resolution' => '300x300',
      'alt' => '',
      'custom_alt' => 1,
      'title' => '',
      'custom_title' => 1,
      'title_type' => 'textfield',
      'default_image' => NULL,
      'use_default_image' => 0,
      'label' => 'Image',
      'weight' => '-3',
      'description' => 'Select an image to upload. Allowed image types are: png, gif, jpg and jpeg. The recommended image size is 640 by 480 pixels. Maximum size is 3000x3000 and the minimum is 300x300.',
      'type' => 'imagefield_widget',
      'module' => 'imagefield',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Embedded audio');
  t('Image');

  return $fields;
}
